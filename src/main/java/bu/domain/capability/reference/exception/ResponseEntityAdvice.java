package bu.domain.capability.reference.exception;

import bu.domain.capability.reference.exception.model.Error;
import bu.domain.capability.reference.exception.model.Errors;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
@Slf4j
public class ResponseEntityAdvice extends ResponseEntityExceptionHandler {

  @Value("${api.error.code.prefix:sample.errors.codes.}")
  private String errorCodePrefix;

  @Value("${api.error.message.prefix:sample.errors.messages.}")
  private String errorMessagePrefix;

  @Autowired
  private Environment environment;


  @ExceptionHandler(value = {ApplicationException.class})
  public ResponseEntity<Errors> applicationException(ApplicationException aexec) {
    return new ResponseEntity<>(
        getErrorResponse(aexec.getErrorCode(), aexec.getErrorMessage(), aexec.getArgsList(),  aexec),
        getHttpStatus(aexec, aexec.getErrorStatus()));
  }

  @ExceptionHandler(value = {BusinessException.class})
  public ResponseEntity<Errors> businessException(BusinessException bexec) {
    return new ResponseEntity<>(
        getErrorResponse(bexec.getErrorCode(), bexec.getErrorMessage(), bexec.getArgsList(), bexec),
        getHttpStatus(bexec, bexec.getErrorStatus()));
  }

  /*@ExceptionHandler({InvalidRequestException.class})
  public ResponseEntity<Errors> handleInvalidRequest(InvalidRequestException ire) {
    Errors errors = new Errors(new ArrayList<>(), MDC.get("traceId"));
    List<FieldError> fieldErrorResponses = ire.getErrors().getFieldErrors();
    for (FieldError fieldError : fieldErrorResponses) {
      String errorKey = fieldError.getDefaultMessage();
      String errorCode = environment.getProperty(errorCodePrefix + errorKey);
      String errorMessage = environment.getProperty(errorMessagePrefix + errorKey);
      getErrorResponse(ire, errorKey, errorCode, errorMessage, errors);
    }
    return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
  }*/

  private void getErrorResponse(
      InvalidRequestException ire, String errorKey, String errorCode, String errorMessage, Errors errors ) {
    Error error = new Error();
    error.setCode(errorCode == null ? errorKey : errorCode);
    error.setMessage(errorMessage == null ? errorKey : errorMessage);
    updateTraceId(errors);
    errors.getErrors().add(error);
  }

  private HttpStatus getHttpStatus(Exception exception, int errHttpStatus) {
    HttpStatus httpStatus;
    try {
      if (errHttpStatus > 199) {
        return HttpStatus.valueOf(errHttpStatus);
      } else {
        return getHttpStatus(exception, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } catch (IllegalArgumentException illegalArgumentException) {
      httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    }
    return getHttpStatus(exception, httpStatus);
  }

  private HttpStatus getHttpStatus(Exception exception, HttpStatus errHttpStatus) {
    HttpStatus httpStatus;
    Annotation statusAnnotation = AnnotationUtils.findAnnotation(exception.getClass(), ResponseStatus.class);
    if (statusAnnotation != null) {
      httpStatus = (HttpStatus) AnnotationUtils.getValue(statusAnnotation);
    } else {
      httpStatus = errHttpStatus;
    }
    return httpStatus;
  }

  private Errors getErrorResponse(String errorCodeParam, String errorMessageParam, String[] args, Exception exec) {
    Errors errorsList = new Errors(new ArrayList<>(), null);
    Error error = new Error();
    String errorCode = environment.getProperty(errorCodePrefix + errorCodeParam);
    String errorMessage = environment.getProperty(errorMessagePrefix + errorCodeParam);
    error.setCode(errorCode == null ? errorCodeParam : errorCode);
    error.setMessage(errorMessage == null ? errorMessageParam : errorMessage);
    updateTraceId(errorsList);
    errorsList.getErrors().add(error);
    return errorsList;
  }

  private void updateTraceId(Errors errorResponse) {
    String traceId = MDC.get("traceId");
    errorResponse.setTraceId(traceId);
  }

}
