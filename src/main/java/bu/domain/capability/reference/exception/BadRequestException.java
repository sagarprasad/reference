package bu.domain.capability.reference.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends BusinessException {
  public BadRequestException(String errorCode, String errorMessage) {
    super(errorCode, errorMessage);
  }

  public BadRequestException(String errorCode, String errorMessage, Throwable throwable) {
    super(errorCode, errorMessage, throwable);
  }

}
