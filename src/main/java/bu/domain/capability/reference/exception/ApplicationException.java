package bu.domain.capability.reference.exception;

import lombok.Getter;

/**
 *
 * This application is creating during application
 * like Database connection / downstream http error /
 *
 */
@Getter
public class ApplicationException extends RuntimeException {

  // u can use this for specific http status code
  // TBD: to create constructor with errorStatus
  private int errorStatus;
  private String errorCode;
  private String errorMessage;
  private Throwable throwable;
  private String[] argsList;

  public ApplicationException(String errorMessage) {
    super(errorMessage);
  }

  public ApplicationException(String errorCode, String errorMessage) {
    super(errorMessage);
    this.errorCode = errorCode;
    this.errorMessage = errorMessage;
  }

  public ApplicationException(String errorCode, String errorMessage, String... args) {
    super(errorMessage);
    this.errorCode = errorCode;
    this.errorMessage = errorMessage;
    this.argsList = args;
  }

  public ApplicationException(String errorCode, String errorMessage, Throwable throwable) {
    super(errorMessage);
    this.errorCode = errorCode;
    this.errorMessage = errorMessage;
    this.throwable = throwable;
  }

  public String[] getArgsList() {
    return this.argsList == null ? null : argsList.clone();
  }
}
