package bu.domain.capability.reference.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
@Getter
public class InvalidRequestException extends BusinessException {

  @SuppressWarnings({"squid:S1948"})
  private Errors errors;

  public InvalidRequestException(String errorMessage) {
    super(errorMessage);
  }

  public InvalidRequestException(String errorCode, String errorMessage) {
    super(errorCode, errorMessage);
  }


  public InvalidRequestException(String errorCode, String errorMessage, Errors errors) {
    super(errorCode, errorMessage);
    this.errors = errors;
  }

  public InvalidRequestException(Errors errors) {
    super("API_ERR_ITEMS_BIND_EXECEPTION", "Invalid Request");
    this.errors = errors;
  }

}
