package bu.domain.capability.reference.exception;


import bu.domain.capability.reference.exception.model.Error;
import bu.domain.capability.reference.exception.model.Errors;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.core.env.Environment;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;


@RestController
@RequestMapping("/error")
@Slf4j
public class SimpleErrorController implements ErrorController {

    private final ErrorAttributes errorAttributes;

    @Autowired
    public SimpleErrorController(ErrorAttributes errorAttributes,
                                 Environment environment) {
        Assert.notNull(errorAttributes, "ErrorAttributes must not be null");
        this.errorAttributes = errorAttributes;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping
    public Errors error(HttpServletRequest aRequest) {
        Map<String, Object> body = getErrorAttributes(aRequest);
        return getErrorResponse(Integer.toString((Integer) body.get("status")), (String) body.get("error"),
                (String) body.get("path"), (String) body.get("trace"));
    }


    private Map<String, Object> getErrorAttributes(HttpServletRequest aRequest) {
        //RequestAttributes requestAttributes = new ServletRequestAttributes(aRequest);
        //Map<String, Object> errorAttributes = this.errorAttributes.getErrorAttributes(requestAttributes, true)
        WebRequest webRequest = new ServletWebRequest(aRequest);
      Map<String, Object> errorAttributes = this.errorAttributes.getErrorAttributes(webRequest, true);
      errorAttributes.put("httpMethod", aRequest.getMethod());
      return errorAttributes;
    }

    private Errors getErrorResponse(String errorCode, String errorMessage, String path, String stackTrace) {
        Errors errors = new Errors(new ArrayList<>(), MDC.get("X-B3-TraceId"));
        Error error = new Error();
        error.setCode(errorCode);
        error.setMessage(error + " " + errorMessage + " " + "Path : " + path);
        errors.getErrors().add(error);
        return errors;
    }

}
