package bu.domain.capability.reference.exception;

import lombok.Getter;

/**
 *
 * This is used when some biz logic fails from the code.
 */

@Getter
public class BusinessException extends RuntimeException {

  // u can use this for specific http status code
  // TBD: to create constructor with errorStatus
  private int errorStatus;
  private String errorCode;
  private String errorMessage;
  private String[] argsList;
  private Throwable throwable;

  public BusinessException(String errorMessage) {
    super(errorMessage);
  }

  public BusinessException(String errorCode, String errorMessage) {
    super(errorMessage);
    this.errorCode = errorCode;
    this.errorMessage = errorMessage;
  }

  public BusinessException(String errorCode, String errorMessage, Throwable throwable) {
    super(errorMessage);
    this.errorCode = errorCode;
    this.errorMessage = errorMessage;
    this.throwable = throwable;
  }

  public BusinessException(String errorCode, String errorMessage, String... args) {
    super(errorMessage);
    this.errorCode = errorCode;
    this.errorMessage = errorMessage;
    this.argsList = args;
  }

}
