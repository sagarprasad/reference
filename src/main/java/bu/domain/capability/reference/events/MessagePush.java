package bu.domain.capability.reference.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessagePush {
    @Autowired
    private GCPMessagePush.PubsubOutboundGateway messagingGateway;

    public void sendMessage(String message) {
        messagingGateway.sendToPubsub(message);
    }
}
