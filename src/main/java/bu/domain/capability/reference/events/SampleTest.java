package bu.domain.capability.reference.events;

import bu.domain.capability.reference.events.model.BaseMessage;
import bu.domain.capability.reference.events.model.Event;
import bu.domain.capability.reference.events.model.SampleEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
//with both gson and jaxson
// More Sample @ - https://github.com/spring-cloud/spring-cloud-gcp
// for Parallel and Stream of messages to process.
public class SampleTest {
    public static void main(String[] args) throws JsonProcessingException {

        Gson gson = new Gson();
        String payload = "{\"eventId\":\"05a1c531-6733-4b98-8410-9dbb3ea416f6\",\"eventType\":\"employeeCreated\",\"entityId\":\"508f200e814c19736de862be\",\"entityType\":\"employees\",\"timestamp\":\"1526395872\",\"datetime\":\"2018-05-15T14:51:12.402+00:00\",\"version\":\"2.0\",\"country\":\"CL\",\"commerce\":\"Falabella\",\"channel\":\"WEB\",\"domain\":\"corp\",\"capability\":\"hrmg\",\"mimeType\":\"application/json\",\"data\":{\"eid\":\"508f200e814c19736de862be\",\"name\":\"John\",\"lastname\":\"Doe\",\"address\":{\"country\":\"Chile\",\"city\":\"Santiago\",\" street\":\"Catedral\",\"street_number\":\"1401\"},\"birthdate\":\"1970-05-20\",\"departmentID\":\"89\",\"email\":\"jdoe@falabella.cl\"}}";
        Event event =  gson.fromJson(payload, Event.class);
        System.out.println(event.getData().toString());
        ObjectMapper objectMapper = new ObjectMapper();
        event =   objectMapper.readValue(payload, Event.class);
        System.out.println(event.getData().toString());
    }
}
