package bu.domain.capability.reference.events.model;

import lombok.Data;

import java.util.Date;

@Data
public abstract class BaseMessage<T> {
    private String eventId;
    private String eventType;
    private String entityId;
    private String entityType;
    private String timestamp;
    private Date datetime;
    private String version;
    private String country;
    private String commerce;
    private String channel;
    private String domain;
    private String capability;
    private String mimeType;
    private T data;
}
