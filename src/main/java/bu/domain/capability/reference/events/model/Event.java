package bu.domain.capability.reference.events.model;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public class Event extends BaseMessage<SampleEvent> {
// override toString method to print minimal values
}
