package bu.domain.capability.reference;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class SampleMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleMicroserviceApplication.class, args);
	}

}
