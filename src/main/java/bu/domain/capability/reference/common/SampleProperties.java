package bu.domain.capability.reference.common;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("sample.custom")
@Component
@Data
public class SampleProperties {
    private boolean enabled = true;
    private String applicationName = "app-name-from-code";
    private List<String> sampleArray = new ArrayList<>();
}
