package bu.domain.capability.reference.common.headers;

import java.util.Map;

public class CorpHeader {

    private final String X_COUNTRY = "x-country";
    private final String X_COMMERCE = "x-commerce";
    private final String X_CHREF = "x-chref";
    private final String X_RHSREF = "x-rhsref";
    private final String X_CMREF = "x-cmref";
    private final String X_TXREF = "x-txref";
    private final String X_PRREF = "x-prref";
    private final String X_USRTX = "x-usrtx";

    private Map<String, String> reqHeaders;

    public CorpHeader(Map<String, String> reqHeaders) {
        this.reqHeaders = reqHeaders;
    }

    public XCountry getXcountry() {
        return XCountry.valueOf(reqHeaders.get(X_COUNTRY));
    }

    public void setXcountry(XCountry xcountry) {
        reqHeaders.put(X_COUNTRY, xcountry.name());
    }

    public XCommerce getXcommerce() {
        return XCommerce.valueOf(reqHeaders.get(X_COMMERCE));
    }

    public void setXcommerce(XCommerce xcommerce) {
        reqHeaders.put(X_COMMERCE, xcommerce.name());
    }

    public XChref getXchref() {
        return XChref.valueOf(reqHeaders.get(X_CHREF));
    }

    public void setXchref(XChref xchref) {
        reqHeaders.put(X_CHREF, xchref.name());
    }

    public String getXrhsref() {
        return reqHeaders.get(X_RHSREF);
    }

    public void setXrhsref(String xrhsref) {
        reqHeaders.put(X_RHSREF, xrhsref);
    }

    public String getXcmref() {
        return reqHeaders.get(X_CMREF);
    }

    public void setXcmref(String xcmref) {
        reqHeaders.put(X_CMREF, xcmref);
    }

    public String getXtxref() {
        return reqHeaders.get(X_TXREF);
    }

    public void setXtxref(String xtxref) {
        reqHeaders.put(X_TXREF, xtxref);
    }

    public String getXprref() {
        return reqHeaders.get(X_PRREF);
    }

    public void setXprref(String xprref) {
        reqHeaders.put(X_PRREF, xprref);
    }

    public String getXusrtx() {
        return reqHeaders.get(X_USRTX);
    }

    public void setXusrtx(String xusrtx) {
        reqHeaders.put(X_USRTX, xusrtx);
    }

    public Map<String, String> getReqHeaders() {
        return reqHeaders;
    }

    public void setReqHeaders(Map<String, String> reqHeaders) {
        this.reqHeaders = reqHeaders;
    }

    private enum XCountry {
        CL("CL"),
        AR("AR"),
        PE("PE"),
        CO("CO"),
        MX("MX"),
        CORP("CORP");

        public final String country;

        XCountry(String country) {
            this.country = country;
        }
    }

    private enum XCommerce {
        Sodimac("Sodimac"),
        Tottus("Tottus"),
        Falabella("Falabella"),
        CORP("CORP");

        public final String xcommerce;

        XCommerce(String xcommerce) {
            this.xcommerce = xcommerce;
        }

    }

    private enum XChref {
        WEB("WEB"),
        Mobile("Mobile"),
        Bot("Bot"),
        POS("POS"),
        Kiosko("Kiosko");

        public final String xchref;

        XChref(String xchref) {
            this.xchref = xchref;
        }
    }

    //static validate method

}
