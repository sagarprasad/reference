package bu.domain.capability.reference.common;

import bu.domain.capability.reference.common.headers.CorpHeader;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Configuration
@ConditionalOnProperty(value = "sample.custom.enable", matchIfMissing = true)
public class SampleConfigurations {
    // beans definations when this is enabled
    static {
        System.out.println("Initializing the block hound ..........");
        //BlockHound.install();
    }

    // can be configure if require in app yaml
    private static List<String> filterHeaders = Arrays.asList("host",
            "content-length",
            "accept",
            "connection");

    @Bean
    @Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public CorpHeader commonHeader(HttpServletRequest request) {
        List<String> headerNames = Collections.list(request.getHeaderNames());
        Map<String, String> headerMap = new HashMap<>();
        headerNames.stream()
                .filter(headerName ->
                        ((headerName != null) &&
                                (!filterHeaders.contains(headerName.toLowerCase())))
                ).forEach(name -> headerMap.put(name, request.getHeader(name)));
        return new CorpHeader(headerMap);
    }

}
