package bu.domain.capability.reference.redis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/redis")
@Slf4j
public class RedisController {

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    InventoryCustomRepository inventoryCustomRepository;

    Map<String, String> User = new HashMap<>();

    @Cacheable(value = "users", key = "#userId")
    @GetMapping("/{userId}")
    public String getUser(@PathVariable String userId) {
        log.info("Getting user with ID {}.", userId);
        return User.get(userId);
    }

    @CachePut(value = "users", key = "#userId")
    @PutMapping("/update")
    public String updatePersonByID(@RequestBody String userId) {
        log.info("updating cache");
        User.put(userId, userId + "Values");
        return User.get(userId);
    }

    @CacheEvict(value = "users", allEntries=true)
    @DeleteMapping("/{userId}")
    public void deleteUserByID(@PathVariable String userId) {
        log.info("deleting person with id {}", userId);
        User.remove(userId);
    }

    @GetMapping("/saveall")
    public String saveAll() {
        log.info("Saving alll");
        Inventory inv1 = new Inventory("LSKU1:N1:NEW", 100, 10, 1, 100-10-1, System.currentTimeMillis(), "HIGH", "{}");
        Inventory inv2 = new Inventory("LSKU1:N2:NEW", 30, 0, 1, 100-10-1, System.currentTimeMillis(), "HIGH", "{}");
        Inventory inv3 = new Inventory("LSKU3:N1:NEW", 25, 13, 1, 100-10-1, System.currentTimeMillis(), "HIGH", "{}");
        Inventory inv4 = new Inventory("LSKU3:N2:NEW", 80, 34, 1, 100-10-1, System.currentTimeMillis(), "HIGH", "{}");
        Inventory inv5 = new Inventory("LSKU5:N1:NEW", 66, 2, 1, 100-10-1, System.currentTimeMillis(), "HIGH", "{}");
        inventoryRepository.saveAll(Arrays.asList(inv1, inv2, inv3, inv4, inv5));
        return "Success";
    }


    @GetMapping("/save")
    public String saveOne() {
        log.info("Saving alll");
        Inventory inv1 = new Inventory("LSKUE:N1:NEW", 99, 1, 1, 100-10-1, System.currentTimeMillis(), "HIGH", "{}");
        inventoryRepository.save(inv1);
        return "Success";
    }

    @GetMapping("/get/{logisticSkuByNode}")
    public Inventory saveOne(@PathVariable String logisticSkuByNode) {
        log.info("Saving alll");
        Optional<Inventory> byId = inventoryRepository.findById(logisticSkuByNode);
        return byId.get();
    }


    @GetMapping("/supplyupdate/{count}")
    public String updateSupply(@PathVariable Integer count) {
        return inventoryCustomRepository.addSupply("LSKU1", "N1", "NEW", count);
    }

    @GetMapping("/demandupdate/{count}")
    public String updateDemand(@PathVariable Integer count) {
        return inventoryCustomRepository.addDemand("LSKU1", "N1", "NEW", count);
    }

}
