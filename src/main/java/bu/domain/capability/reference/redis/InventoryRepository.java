package bu.domain.capability.reference.redis;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

interface InventoryRepository extends CrudRepository<Inventory, String>, QueryByExampleExecutor<Inventory> {
}
