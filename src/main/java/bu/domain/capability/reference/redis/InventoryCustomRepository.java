package bu.domain.capability.reference.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class InventoryCustomRepository {

    private HashOperations hashOperations;

    @Autowired
    public InventoryCustomRepository(RedisTemplate redisTemplate) {
        hashOperations = redisTemplate.opsForHash();
    }

    public String addSupply(String logisticSku, String node, String condition, int count) {
        String key = "atp" + ":" + logisticSku + ":" +  node + ":" +  condition;
        // transaction
        // update ATP ->
        // update timeStamp ->
        Long atp = hashOperations.increment(key, "supply", count);
        // update TTLs
        // End Transaction
        return atp.toString();
    }

    public String addDemand(String logisticSku, String node, String condition, int count) {
        String key = "atp" + ":" + logisticSku + ":" +  node + ":" +  condition;
        // transaction
        // update ATP ->
        // update timeStamp ->
        Long atp = hashOperations.increment(key, "demand", count);
        // update TTLs
        // End Transaction
        return atp.toString();
    }
    // TTLs
    // FLUSH-ALL
    // FLUSH ONE LOGISTIC SKU

    private static String keybuilder(String logisticSku, String node, String condition) {
        return  "atp" + ":" +
                logisticSku + ":" +
                node + ":" +
                condition;
    }
}
