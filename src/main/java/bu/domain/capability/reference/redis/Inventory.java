package bu.domain.capability.reference.redis;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@Data
@RedisHash("atp")
@NoArgsConstructor
@AllArgsConstructor
public class Inventory {

    // L1:N1:NEW
    //logisticSku:Node:Condition
    private @Id
    String logisticSkuByNode;
    //@Indexed - Secondary Index - do we need ?
    private Integer supply;
    private Integer demand;
    private Integer safety;
    // supply - demand - safety
    private Integer atp;
    // updated time
    private Long utime;
    // qualatative analysis - OOS,LOW,HIGH
    private String qualatative;
    // any additional data in json - or Object Here
    private String additionalData;

}
