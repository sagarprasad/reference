package bu.domain.capability.reference.resource;

import bu.domain.capability.reference.common.SampleProperties;
import bu.domain.capability.reference.events.MessagePush;
import bu.domain.capability.reference.exception.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping
@Slf4j
public class SampleRestService {

    @Autowired
    SampleProperties properties;

    @Autowired
    MessagePush messagePush;

    private final String payload = "{\"eventId\":\"05a1c531-6733-4b98-8410-9dbb3ea416f6\",\"eventType\":\"employeeCreated\",\"entityId\":\"508f200e814c19736de862be\",\"entityType\":\"employees\",\"timestamp\":\"1526395872\",\"datetime\":\"2018-05-15T14:51:12.402+00:00\",\"version\":\"2.0\",\"country\":\"CL\",\"commerce\":\"Falabella\",\"channel\":\"WEB\",\"domain\":\"corp\",\"capability\":\"hrmg\",\"mimeType\":\"application/json\",\"data\":{\"eid\":\"508f200e814c19736de862be\",\"name\":\"John\",\"lastname\":\"Doe\",\"address\":{\"country\":\"Chile\",\"city\":\"Santiago\",\" street\":\"Catedral\",\"street_number\":\"1401\"},\"birthdate\":\"1970-05-20\",\"departmentID\":\"89\",\"email\":\"jdoe@falabella.cl\"}}";

    @GetMapping("/status")
    public Map status() {
        log.info("Status Method is called");
        Map<String, String> response = new HashMap<>();
        response.put("status", "ok -" + properties.getApplicationName());
        messagePush.sendMessage(payload);
        log.info("Sent message successful !!!");
        return response;
    }

    @GetMapping("/err")
    public void err(@RequestParam(name = "code", required = false) String code) {
        log.info("Error Method is called for code {}", code);
        switch (code) {
            case "400" : throw new BadRequestException("code", "bad request");
            case "404" : throw new NotFoundException("SAMPLE_404", "SAMPLE_404");
            case "500" : throw new ApplicationException("code", "some random exception");
            default:
                throw new BadRequestException("code", "message");
        }
    }

}
