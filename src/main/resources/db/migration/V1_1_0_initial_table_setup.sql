CREATE TABLE employee (
  emp_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  name VARCHAR,
  age VARCHAR,
  company VARCHAR DEFAULT 'GOOGLE',
  location VARCHAR,
  sample JSONB,
  created_at timestamp with time zone DEFAULT now(),
  updated_at timestamp with time zone DEFAULT now(),
  UNIQUE (emp_id)
);
CREATE INDEX idx_age ON employee (age);
